#pragma once
#include <gio/gio.h>
#include <map>
#include <giomm/file.h>
#include <giomm/init.h>
#include <iostream>

namespace FileTools {
	template<class T>
class RecursiveDirectoryMonitor : public sigc::trackable {
		protected:
			T m_callBack;
			Glib::RefPtr<Gio::File> m_rootFolder;
			std::vector<Glib::RefPtr<Gio::FileMonitor>> m_monitors;
		public:
			RecursiveDirectoryMonitor(Glib::RefPtr<Gio::File>&& rootFolder, const T& callback) :
                    m_rootFolder(std::move(rootFolder)),
                    m_callBack(callback)
            {
				rebuildDirectoryMap();
			}

			void rebuildDirectoryMap()
            {
				m_monitors.clear();
				handleFile(m_rootFolder->get_parent(), m_rootFolder->query_info());
				addSubfolder(m_rootFolder);
				std::cout << "dirmap is that big: " << m_monitors.size();
				std::cout << std::endl;
			}

			void addSubfolder(const Glib::RefPtr<Gio::File>& folder)
            {
                try
                {
                    auto fileIt = folder->enumerate_children();
                    while (auto fileInfo = fileIt->next_file())
                    {
                        handleFile(folder, fileInfo);
                    }
                }
                catch (const Glib::Error &e)
                {
                    std::cerr << e.what();
                }
            }

            void handleFile(const Glib::RefPtr<Gio::File> &folder, const Glib::RefPtr<Gio::FileInfo> &fileInfo) {
                auto file = getFileFromFileInfo(folder, fileInfo);
                switch (fileInfo->get_file_type()) {
                    case Gio::FILE_TYPE_DIRECTORY:
                        std::cout << "found dir: " << fileInfo->get_name();
                        addMonitor(file);
                        addSubfolder(file);
                        break;
                    default:
                        break;
                }
            }

            void addMonitor(const Glib::RefPtr<Gio::File>& file) {
			    std::cout << "adding monitor: " << file->get_parse_name() << '\n';
                auto monitor = file->monitor(Gio::FILE_MONITOR_WATCH_MOUNTS);
                monitor->signal_changed().connect(sigc::mem_fun(this, &RecursiveDirectoryMonitor::onFileChanged));
				m_monitors.emplace_back(monitor);
			}

			void onFileChanged(const Glib::RefPtr<Gio::File>& oldFile,const Glib::RefPtr<Gio::File>& newFile,Gio::FileMonitorEvent monitorEvent) {
				std::cerr << "FILE CHANGED!\n" << std::endl;

			    m_callBack(oldFile, newFile, monitorEvent);
                rebuildDirectoryMap();

			}

	protected:
		Glib::RefPtr<Gio::File> getFileFromFileInfo(const Glib::RefPtr<Gio::File>& currentFolder, const Glib::RefPtr<Gio::FileInfo>& fileInfo) {
			auto name = fileInfo->get_name();
			auto path = currentFolder->get_path() + '/' + name;
			return Gio::File::create_for_path(path);
		}
	};
}
